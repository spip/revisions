# Changelog

## 3.2.0 - 2024-11-27

### Added

- Installable en tant que package Composer

### Changed

- Utiliser `#LAYOUT_PRIVE` à la place de `#LARGEUR_ECRAN`
- Compatible SPIP 5.0.0-dev

### Fixed

- spip/spip#5460 Utiliser les propriétés logiques dans la CSS de l'espace privé
- #4860 Rétablir la possibilité de désactiver les révisions sur tous les objets
- HTML5: Retrait des `CDATA` et `text/javascript` dans les balises `<script>`

### Removed

- spip/spip#5994 Code dérogatoire interne pour gérer la balise `<math>`
